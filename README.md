#### Build
```shell
$ GOOS=js GOARCH=wasm go build -o main.wasm
```

#### Bootstrap wasm_exec.js
```shell
$ cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" .
```

#### Start serving
```shell
$ goexec 'http.ListenAndServe(":8080", http.FileServer(http.Dir(".")))'
```

> above step requires to `go get -u -v github.com/shurcooL/goexec`

Goto http://localhost:8080
